package com.social.console;

import com.social.console.data.CommandInput;

/**
 * Interface for Controllers
 */
public interface ICommand {

	/**
	 * Method to be implemented for the following commands
     * posting: <user name> -> <message>
     * reading: <user name>
     * following: <user name> follows <another user>
     * wall: <user name> wall
	 * 
	 * @param commandInput
	 */
	public abstract void commandPerformed(CommandInput commandInput);

}