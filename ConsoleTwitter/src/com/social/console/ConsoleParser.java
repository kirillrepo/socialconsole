package com.social.console;

import com.social.console.data.Command;
import com.social.console.data.CommandInput;

/**
 *
 *Class to parse user input string to @CommandInput 
 *
 */
public class ConsoleParser {
	    private static final String POST_PROMPT="->";
	    private static final String FOLLOWS= "follows";
	    private static final String WALL= "wall";
		
		public static CommandInput parse(String consoleInput){
			
			CommandInput commandInput = null;
			
			
			if (consoleInput !=null){
				int postIx = consoleInput.indexOf(POST_PROMPT);
				int followsIx = consoleInput.indexOf(FOLLOWS);
				int wallIx = consoleInput.indexOf(WALL);
				String[] helper = consoleInput.split(" ");
				if (postIx>0){
					String userName1 = helper[0].trim(); 
					String message=consoleInput.substring(postIx+1).trim();
					commandInput = new CommandInput(userName1, Command.posting);
					commandInput.setMessage(message);
					
				}else if ( followsIx>0 ){
					String userName1 = helper[0].trim(); 
					String userName2 = helper[2].trim();
					commandInput = new CommandInput(userName1, Command.following);
					commandInput.setUserFollow(userName2);
				}else if (wallIx>0){
					String userName1 = helper[0].trim(); 
					commandInput = new CommandInput(userName1, Command.wall);
				}else{
					commandInput = new CommandInput(consoleInput.trim(), Command.reading);
				}
				
			}
			
			
			return commandInput;
		}

}
