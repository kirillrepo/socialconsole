package com.social.console;

import java.io.IOException;

import com.social.console.view.PostingView;

/**
 * Main class to run console-based version of a Twitter-like social networking application 
 * 
 * General requirements
 * Application must use the console for input and output;
 * User submits commands to the application:
 * posting: <user name> -> <message>
 * reading: <user name>
 * following: <user name> follows <another user>
 * wall: <user name> wall
 * Don't worry about handling any exceptions or invalid commands. Assume that the user will always type the correct commands. Just focus on the sunny day scenarios.
 */
public class ClientApp {
	
	/**
	 * Method to setup simplified MVC and start user input loop
	 * 
	 * @throws IOException
	 * 
	 */
	public void runConsole() throws IOException{
		
		PostingView view = new PostingView();
		ICommand controller = new Controller(view);
		view.addCommandListener(controller);
		
		boolean exitFlag = false;
		
		while(exitFlag==false)
		{
			view.prompt();
			view.readInput(System.in);
			
		}
		
	}

	public ClientApp() {
	}

	public static void main(String[] args) {
		
		ClientApp  client = new ClientApp();
		try {
			client.runConsole();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}

}
