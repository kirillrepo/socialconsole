package com.social.console;

import java.util.ArrayList;
import java.util.List;

import com.social.console.data.Command;
import com.social.console.data.CommandInput;
import com.social.console.data.Message;
import com.social.console.data.Repository;
import com.social.console.data.User;
import com.social.console.view.PostingView;

/**
 * 
 * Simple Controller class
 *
 */
/**
 * @author kirill
 *
 */
public class Controller implements ICommand {
	
	private PostingView view;
	private Repository repository = Repository.getRepository();
	
	

	
	/**
	 * Controller constructor with injected view
	 * 
	 * @param view
	 */
	public Controller(PostingView view) {
		super();
		this.view = view;
	}


	/*
	 * Implementation of the commands
     * posting: <user name> -> <message>
     * reading: <user name>
     * following: <user name> follows <another user>
     * wall: <user name> wall
     * 
	 * forward messages to View to display
	 * 
	 */
	@Override
	public void commandPerformed(CommandInput commandInput) {
		List<Message> messages=new ArrayList<Message>(); 
		boolean showUser = false;
		
		if (commandInput.getCommand()==Command.posting){
			User user = repository.addUser(commandInput.getUser());
			Message message = new Message(user,commandInput.getMessage() );
			repository.addMessage(message);
			
		}else if (commandInput.getCommand()==Command.reading){
			User user = repository.getUser(commandInput.getUser());
			if (user != null){
				messages =repository.getMessage(user);
			}
		}else if (commandInput.getCommand()==Command.following){
			User user = repository.getUser(commandInput.getUser());
			User userFollow = repository.getUser(commandInput.getUserFollow());
			if (user != null && userFollow != null )
			{
				user.getFollows().add(userFollow);
			}
		}else if (commandInput.getCommand()==Command.wall){
			User user = repository.getUser(commandInput.getUser());
			
			if (user != null)
			{
				messages.addAll(repository.getMessage(user));
				for (User follow : user.getFollows()) {
					messages.addAll(repository.getMessage(follow));
				} 
			}
			showUser = true;
		}
			
		view.display(messages,showUser); // forward messages to View to display

	}
	
	

}
