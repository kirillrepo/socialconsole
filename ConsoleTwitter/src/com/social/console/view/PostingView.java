/**
 * 
 */
package com.social.console.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.social.console.ConsoleParser;
import com.social.console.ICommand;
import com.social.console.data.CommandInput;
import com.social.console.data.Message;

/**
 * View implementation 
 *
 */
public class PostingView {
	private static final String INPUT_PROMPT=">";
	
	private ICommand action;
	

	/**
	 * Default constructor 
	 */
	public PostingView() {
	}

	
	/**
	 * 
	 * Method to sort, format and display messages
	 * @param messages
	 * @param showUser
	 */
	public void display(List<Message> messages, boolean showUser) {
		if (messages == null || messages.isEmpty())
		{
			System.out.println("");
		}else
		{
			Collections.sort(messages, new Comparator<Message>(){  
	            public int compare(Message d1, Message d2) {
	                return d2.getCreated().compareTo(d1.getCreated());
	            }
			});
			
			long now = Calendar.getInstance().getTime().getTime();
			
			for (Message message : messages) {
			
				long interval = now - message.getCreated().getTime();
				
				long timeDiff = TimeUnit.SECONDS.convert(interval, TimeUnit.MILLISECONDS);
				String unit =" seconds ago";
				if (Math.abs(timeDiff)<60){
					unit =" seconds ago";
				}else if (Math.abs(timeDiff)<3600){
					timeDiff = TimeUnit.MINUTES.convert(interval, TimeUnit.MILLISECONDS);
					unit =" minutes ago";
				}else {
					timeDiff = TimeUnit.HOURS.convert(interval, TimeUnit.MILLISECONDS);
					unit =" hours ago";
				}

				String prefix =showUser?message.getUser().getUserName()+" - ":"";
				System.out.printf("%s ( %d%s )%n",prefix+message.getPayload(),timeDiff,unit);
			}
			
		}

	}
	
	public void prompt(){
		System.out.print(INPUT_PROMPT);
	}
	
	/**
	 * 
	 * Method to add Controller as "listener"
	 * @param action
	 */
	public void addCommandListener(ICommand action){
		this.action=action;
	}

	/**
	 * 
	 * Method to read user input from stream
	 * Parse the input and submit it to Controller
	 * @param input
	 * @throws IOException
	 */
	public void readInput(InputStream input) throws IOException {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
    	String consoleInput = reader.readLine();
		CommandInput  commandInput = ConsoleParser.parse(consoleInput);
		action.commandPerformed(commandInput);
	}

}
