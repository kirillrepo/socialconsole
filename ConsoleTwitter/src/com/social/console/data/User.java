/**
 * 
 */
package com.social.console.data;

import java.util.HashSet;
import java.util.Set;

/**
 * Class to hold User info
 *
 */
public class User {
	
	private String user;
	private Set<User> follows = new HashSet<User>();


	/**
	 * Constructor to create User 
	 */
	public User(String user) {
		super();
		this.user = user;
	}

	public User() {
	}
	
	
	public String getUserName() {
		return user;
	}

	public Set<User> getFollows() {
		return follows;
	}
	
	public void addFollows(User user){
		this.follows.add(user);
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	
	

}
