package com.social.console.data;


/**
 * Class to hold parsed user input
 *
 */
public class CommandInput {

	private String user;
	private String message;
	private Command command;
	private String userFollow;

	public CommandInput(String user, Command command) {
		super();
		this.user = user;
		this.command = command;
	}
	
    /*
     * 
     * Getters and setters
     * 	
     */
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Command getCommand() {
		return command;
	}



	public void setCommand(Command command) {
		this.command = command;
	}


	public String getUserFollow() {
		return userFollow;
	}


	public void setUserFollow(String userFollow) {
		this.userFollow = userFollow;
	}
	
	
	@Override
	public String toString() {
		return "CommandInput [user=" + user + ", message=" + message
				+ ", command=" + command + "]";
	}
	
	
	
}
