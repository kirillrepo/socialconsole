/**
 * 
 */
package com.social.console.data;

import java.util.Calendar;
import java.util.Date;

/**
 * Class to hold message
 *
 */
public class Message {
	
	private String payload;
	private User user;
	private Date created;

	
	/**
	 * Constructor to create Message
	 * 
	 * @param user
	 * @param payload
	 */
	public Message(User user, String payload ) {
		super();
		this.payload = payload;
		this.user = user;
		this.created = Calendar.getInstance().getTime();
	}


	public Message() {
	}
	

    /*
     * 
     * Getters and setters
     * 	
     */
	
	public String getPayload() {
		return payload;
	}


	public void setPayload(String payload) {
		this.payload = payload;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Date getCreated() {
		return created;
	}


	public void setCreated(Date created) {
		this.created = created;
	}



}
