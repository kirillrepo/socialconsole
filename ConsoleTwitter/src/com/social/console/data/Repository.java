package com.social.console.data;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * 
 * Class to store @User and @Message
 * Implemented as Singleton
 *
 */
public class Repository {
	
	private static Repository repository = new Repository();
	
	private Map<String,User> users = new Hashtable<String,User>();
	
	private Map<User,List<Message>> messages = new Hashtable<User,List<Message>>();

	private Repository() {
	}
	
	public User addUser(String userName){
		User user = users.get(userName);
		if (user == null){
			user = new User(userName);
			users.put(userName, user);
		}
		return user;
	}
	
	public void addMessage(Message msg){
		
		User key = msg.getUser();
		List<Message> lst = messages.get(key);
		if (lst == null){
			lst = new ArrayList<Message>();
			messages.put(key, lst);
		}
		
		lst.add(msg);
		
	}

	public static Repository getRepository(){
		return repository;
	}

	public User getUser(String userName) {
		return users.get(userName);
	}

	public List<Message> getMessage(User user) {
		return messages.get(user);
	}

}
