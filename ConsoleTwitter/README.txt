
Simplified, console-based version of a Twitter-like social networking application 
implemented in loopback mode (no server)

1.General requirements
Application must use the console for input and output;
User submits commands to the application:
posting: <user name> -> <message>
reading: <user name>
following: <user name> follows <another user>
wall: <user name> wall

Don't worry about handling any exceptions or invalid commands. 
Assume that the user will always type the correct commands. Just focus on the sunny day scenarios.

2. Compile .java sources under src directory
   There are no external dependencies
   The main method is in com/social/console/ClientApp.java:
   java com/social/console/ClientApp

3. Console example:

>Alice -> I love the weather today

>Bob -> Oh, we lost!

>Bob -> at least it's sunny

>Alice
> I love the weather today ( 45 seconds ago )
>Bob
> at least it's sunny ( 21 seconds ago )
> Oh, we lost! ( 35 seconds ago )
>Charlie -> I'm in New York today! Anyone wants to have a coffee?

>Charlie follows Alice

>Charlie wall
Charlie - > I'm in New York today! Anyone wants to have a coffee? ( 23 seconds ago )
Alice - > I love the weather today ( 1 minutes ago )
>Charlie follows Bob

>Charlie wall
Charlie - > I'm in New York today! Anyone wants to have a coffee? ( 52 seconds ago )
Bob - > at least it's sunny ( 1 minutes ago )
Bob - > Oh, we lost! ( 2 minutes ago )
Alice - > I love the weather today ( 2 minutes ago )
>